import React, {useState} from 'react'
import {InputContainer, InputTitle, Input, ShowHideButton} from "./styled";

export const InputComponent = ({
  value,
  setValue,
  title,
  isPassword,
  mask,
  mobile,
}) => {
  const [val, setVal] = useState(value);
  const [show, setShow] = useState(false);

  const handleChange = (val) => {
    setVal(val);
    setValue(val);
  }

  return (
    <InputContainer mobile={mobile}>
      <InputTitle mobile={mobile}>{title}</InputTitle>
      <Input
        mobile={mobile}
        mask={mask}
        value={val}
        onChange={(e) => handleChange(e.target.value)}
        type={isPassword && !show ? 'password' : 'text'}
      />
      {isPassword &&
      <ShowHideButton mobile={mobile} onClick={() => setShow(!show)}>{show ? 'Скрыть' : 'Показать'}</ShowHideButton>
      }
    </InputContainer>
  )
}
