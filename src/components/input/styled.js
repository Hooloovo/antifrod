import styled from 'styled-components';
import InputMask from 'react-input-mask';

export const InputContainer = styled.div`
    display: flex;
    flex-direction: column;
    
    margin-bottom: 24px;
    height: ${({ mobile }) => mobile ? '67px' : '97px'};
    position: relative;
`

export const InputTitle = styled.span`
    font-family: Inter;
    font-style: normal;
    font-weight: 500;
    font-size: ${({ mobile }) => mobile ? '14px' : '16px'};
    line-height: ${({ mobile }) => mobile ? '17px' : '19px'};
    margin-bottom: 14px;
`

export const Input = styled(InputMask)`
    max-width: 426px;
    width: 100%;
    height: ${({ mobile }) => mobile ? '40px' : '64px'};
    border: 1px solid #8692A6;
    border-radius: 6px;
    box-sizing: border-box;
    padding: ${({ mobile }) => mobile ? '12px 68px 12px 15px' : '23px 85px 23px 30px'};
    font-family: Inter;
    font-style: normal;
    font-weight: 500;
    font-size: ${({ mobile }) => mobile ? '12px' : '14px'};
    line-height: ${({ mobile }) => mobile ? '15px' : '17px'};
    
    &:focus {
        box-shadow: 0px 4px 10px 3px rgba(0, 0, 0, 0.11);
        border: 1px solid #1565D8;
        outline: none;
    }
`

export const ShowHideButton = styled.button`
    position: absolute;
    bottom: ${({ mobile }) => mobile ? '7px' : '22px'};
    right: ${({ mobile }) => mobile ? '7px' : '24px'};
    
    border: none;
    outline: none;
    background-color: transparent;
    cursor: pointer;
    
    font-family: Inter;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 15px;
    color: #000;
`

export const Checkbox = styled.input`
    width: 17px;
    height: 17px;
`

export const RememberContainer = styled.div`
    display: flex;
    align-items: center;
    
    margin-bottom: 39px;
`

export const RememberTitle = styled.span`
    font-family: Inter;
    font-style: normal;
    font-weight: 400;
    font-size: ${({ mobile }) => mobile ? '12px' : '16px'};
    line-height: ${({ mobile }) => mobile ? '15px' : '19px'};
    
    color: #696F79;
    
    margin-left: 8px;
    padding-top: 1px;
`

export const SubmitButton = styled.button`
    max-width: 426px;
    width: 100%;
    height: ${({ mobile }) => mobile ? '40px' : '56px'};
    background: ${({active}) => active ? '#1565D8' : '#E0E0E0'};
    border: none;
    border-radius: 6px;
    font-family: Inter;
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    display: flex;
    align-items: center;
    text-align: center;
    justify-content: center;
    
    color: #FFFFFF;
    outline: none;
    cursor: ${({active}) => active ? 'pointer' : 'default'};
`
