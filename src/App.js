import React from 'react';
import {IndexPage} from "./login";
import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink } from '@apollo/client';

const link = createHttpLink({
  uri: 'http://194.87.236.38/graphql',
});

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <IndexPage />
    </ApolloProvider>
  );
}

export default App;
