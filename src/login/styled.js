import styled from 'styled-components';

export const PageContainer = styled.div`
    @media (max-width: 900px) {
      display: none;
    }
    
    width: 100%;
    min-height: 100vh;
    display: grid;
    grid-template-columns: 1fr 1fr;
    
    > div {
        position: relative;
        width: 100%;
        height: 100%;
        min-height: 100vh;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        box-sizing: border-box;
        padding: 0 48px;
    }
`;

export const LoginFormContainer = styled.div`
    height: 448px;
    width: 100%;
    max-width: 428px;
    display: flex;
    flex-direction: column;
`;

export const EnterTitle = styled.span`
    font-family: Inter;
    font-style: normal;
    font-weight: bold;
    font-size: ${({ mobile }) => mobile ? '24px' : '30px'};
    line-height: ${({ mobile }) => mobile ? '29px' : '36px'};
    display: flex;
    align-items: center;
    
    margin-bottom: 16px;
`;

export const Hr = styled.div`
    height: 1px;
    width: 100%;
    border-bottom: 1px solid #F5F5F5;
    content: ' ';
    
    margin-bottom: ${({ mobile }) => mobile ? '32px' : '24px'};
`;

export const RightSideImage = styled.img`
    position: absolute;
    width: 100%;
    height: 100%;
    object-fit: cover;
    z-index: 1;
`

export const RightSideFilter = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 2;
    background: linear-gradient(0deg, rgba(21, 101, 216, 0.9), rgba(21, 101, 216, 0.9));
`

export const LogoContainer = styled.div`
    position: absolute;
    top: 70px;
    left: 88px;
    display: flex;
    align-items: center;
    z-index: 10;
`

export const LogoText = styled.span`
    font-family: Helvetica;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 18px;
    display: flex;
    align-items: center;
    color: #FFFFFF;
    
    margin-left: 17px;
`

export const PreviewTextContainer = styled.div`
    width: 100%;
    max-width: 473px;
    min-width:
    display: flex;
    flex-direction: column;
    z-index: 10;
`

export const Quotes = styled.img`
    margin-bottom: 40px;
    align-self: flex-start;
`

export const EndQuotes = styled.img`
    margin-top: 98px;
    align-self: flex-end;
`

export const PreviewText = styled.span`
    font-family: Inter;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 38px;
    
    color: #FFFFFF;
    
    margin-bottom: 98px;
`
