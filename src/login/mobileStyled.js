import styled from 'styled-components'

export const MobileContainer = styled.div`
  @media (min-width: 899px) {
    display: none;
  }
  width: 100vw;
  min-height: 100vh;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const MobileBackgroundImage = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
  z-index: 1
`

export const MobileBackgroundFilter = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: linear-gradient(0deg, rgba(21, 101, 216, 0.9), rgba(21, 101, 216, 0.9));
  z-index: 2;
  content: ' ';
`

export const Layout = styled.div`
  z-index: 10;
  width: 261px;
  display: flex;
  flex-direction: column;
`

export const MobileLogo = styled.img`
  margin: 34px 0;
  align-self: flex-start;
`

export const MobileLoginContainer = styled.div`
  width: 100%;
  background: #FFF;
  border-radius: 12px;
  
  display: flex;
  flex-direction: column;
  padding: 32px 17px 24px 17px;
  box-sizing: border-box;
`
