import React, {useState} from 'react'
import axios from 'axios'
import {
  EndQuotes,
  EnterTitle,
  Hr,
  LoginFormContainer, LogoContainer,
  PageContainer, PreviewText, PreviewTextContainer, Quotes,
  RightSideFilter,
  RightSideImage
} from "./styled";
import {InputComponent} from "../components/input";
import {SubmitButton} from "../components/input/styled";
import logo from './img/logo.svg'
import quotes from './img/quotes.svg'
import endquotes from './img/endQuotes.svg'
import {
  Layout,
  MobileBackgroundFilter,
  MobileBackgroundImage,
  MobileContainer,
  MobileLoginContainer,
  MobileLogo
} from "./mobileStyled";
import { gql, useMutation } from '@apollo/client';

const SET_AUTH_DATA = gql`
  mutation Signin($phone: String!, $password: String!) {
    signin(input: { phone:$phone, password:$password}) {
      id
      phone
    }
  }
`

export const IndexPage = () => {
  const [phone, setPhone] = useState('+7');
  const [password, setPassword] = useState('');
  const [code, setCode] = useState('');
  const [stage, setStage] = useState(1);
  const [setAuthData] = useMutation(SET_AUTH_DATA);

  const handleClick = () => {
    phone !== '' && password !== '' ? setStage(2) : setStage(1);
    setAuthData({ variables: { phone: phone, password: password}})
      .then(result => console.log(result));
  }

  return (
    <>
      <PageContainer>
        <div>
          <LoginFormContainer>
            <EnterTitle>Вход</EnterTitle>
            <Hr/>
            {stage === 1 ? (
              <>
                <InputComponent
                  mask={'+7 (999) 999-99-99'}
                  key={1}
                  value={phone}
                  setValue={(val) => setPhone(val)}
                  title={'Введите ваш номер телефона'}
                />
                <InputComponent
                  key={2}
                  isPassword={true}
                  value={password}
                  setValue={(val) => setPassword(val)}
                  title={'Пароль'}
                />
                {/*<RememberContainer>*/}
                {/*  <Checkbox*/}
                {/*    type="checkbox"*/}
                {/*  />*/}
                {/*  <RememberTitle>Запомнить меня</RememberTitle>*/}
                {/*</RememberContainer>*/}
                <SubmitButton
                  onClick={() => handleClick()}
                  active={phone !== '' && password !== ''}
                >
                  Войти
                </SubmitButton>
              </>
            ) : (
              <>
                <InputComponent
                  key={3}
                  value={code}
                  setValue={(val) => setCode(val)}
                  title={'Код из SMS'}
                />
                <SubmitButton
                  onClick={() => handleClick()}
                  active={phone !== '' && password !== ''}>
                  Войти
                </SubmitButton>
              </>
            )}
          </LoginFormContainer>
        </div>
        <div>
          <RightSideImage
            src={'https://s3-alpha-sig.figma.com/img/289f/6ad2/555a3eed00261ea01098aea9161a886f?Expires=1603065600&Signature=b710uRZq-cUiQTRFmG9BUnf3VoiSY3Y4no7hMjGOQFhI9UWDktHnrB3qU7-iZs0RKWfMERBv9veZw8OrdinbkJDPwzpVQqfsTzKbSnu1JIdw8YhmH68g5Pyv4iQP9RAwrT6L84YQTA24lGA-L3V~SD2aAkXdNQprad0DEVkATz2~1ddeB4O2PI30NysTNjtEVaT-nBh2Cl37ZAmacnnHyJXo~nRgcygasJFqDKI9r8EzIEXjzMZSUDmwzF0x4fBbRlYeyiblQhJ6Zpx4~VYJHTawIBKr1NsxQduYlC-g5qF52vXWNCD8~YnRKAaUgbOz0tBLrvl3yZ-Dz~tGTP8kvg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'}
          />
          <RightSideFilter/>
          <LogoContainer>
            <img src={logo}/>
          </LogoContainer>
          <PreviewTextContainer>
            <Quotes
              src={quotes}
            />
            <PreviewText>
              Мы помогаем людям воплощать их планы, создавая лучшие финансовые решения.
              Мы — команда профессионалов, работающих для наших клиентов и всей страны.
            </PreviewText>
            <div style={{display: 'flex', alignSelf: 'flex-end', flexDirection: 'row-reverse'}}>
              <EndQuotes
                src={endquotes}
              />
            </div>
          </PreviewTextContainer>
        </div>
      </PageContainer>
      <MobileContainer>
        <MobileBackgroundImage
          src={'https://s3-alpha-sig.figma.com/img/289f/6ad2/555a3eed00261ea01098aea9161a886f?Expires=1603065600&Signature=b710uRZq-cUiQTRFmG9BUnf3VoiSY3Y4no7hMjGOQFhI9UWDktHnrB3qU7-iZs0RKWfMERBv9veZw8OrdinbkJDPwzpVQqfsTzKbSnu1JIdw8YhmH68g5Pyv4iQP9RAwrT6L84YQTA24lGA-L3V~SD2aAkXdNQprad0DEVkATz2~1ddeB4O2PI30NysTNjtEVaT-nBh2Cl37ZAmacnnHyJXo~nRgcygasJFqDKI9r8EzIEXjzMZSUDmwzF0x4fBbRlYeyiblQhJ6Zpx4~VYJHTawIBKr1NsxQduYlC-g5qF52vXWNCD8~YnRKAaUgbOz0tBLrvl3yZ-Dz~tGTP8kvg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'}/>
        <MobileBackgroundFilter/>
        <Layout>
          <MobileLogo src={logo}/>
          <MobileLoginContainer>
            <EnterTitle mobile>Вход</EnterTitle>
            <Hr mobile/>
            {stage === 1 ?
              (<>
                <InputComponent
                  mobile
                  mask={'+7 (999) 999-99-99'}
                  key={1}
                  value={phone}
                  setValue={(val) => setPhone(val)}
                  title={'Телефон'}
                />
                <InputComponent
                  mobile
                  isPassword={true}
                  key={2}
                  value={password}
                  setValue={(val) => setPassword(val)}
                  title={'Пароль'}
                />
                {/*<RememberContainer>*/}
                {/*  <Checkbox*/}
                {/*    type="checkbox"*/}
                {/*  />*/}
                {/*  <RememberTitle>Запомнить меня</RememberTitle>*/}
                {/*</RememberContainer>*/}
              </>) : (
                <>
                  <InputComponent
                    mobile
                    key={3}
                    value={code}
                    setValue={(val) => setCode(val)}
                    title={'Код'}
                  />
                </>
              )}
            <SubmitButton
              mobile
              onClick={() => handleClick()}
              active={phone !== '' && password !== ''}
            >
              Войти
            </SubmitButton>
          </MobileLoginContainer>
        </Layout>
      </MobileContainer>
    </>
  )
}
